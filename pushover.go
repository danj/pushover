package pushover

import (
	"errors"
	"net/http"
	"net/url"
	"strings"
	"time"
)

type Pushover struct {
	token string
	user  string
}

// New returns a pushover config.
func New(token, user string) (*Pushover, error) {
	if token == "" || user == "" {
		return nil, errors.New("Missing credentials")
	}
	return &Pushover{token: token, user: user}, nil
}

// Notify sends a notification.
func (po Pushover) Notify(title, message string) error {
	client := &http.Client{}

	client.Timeout = 10 * time.Second

	form := url.Values{}
	form.Add("expire", "36000")
	form.Add("retry", "90")
	form.Add("title", title)
	form.Add("message", message)
	form.Add("token", po.token)
	form.Add("user", po.user)
	form.Add("priority", "1")

	req, _ := http.NewRequest("POST", "https://api.pushover.net/1/messages.json",
		strings.NewReader(form.Encode()))

	req.PostForm = form
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	_, err := client.Do(req)
	if err != nil {
		return err
	}
	return nil

	// to debug
	//r, err := client.Do(req)
	//if err != nil {
	//	log.Fatalf("client failed: %v", err)
	//}
	//fmt.Println(req)
	//fmt.Println(r)
}
